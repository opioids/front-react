import axios from 'axios';

axios.defaults.baseURL = 'http://82.146.61.253:8585/secure_api_v1/';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = 'application/x-www-form-urlencoded';
axios.defaults.headers.common['Content-Type'] = '*';

export default axios;
