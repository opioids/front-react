import React, { Component } from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch, Link} from 'react-router-dom';

import { AddForm, Researches } from 'components';
import { PostsList, FullPost } from 'modules';




class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
      isDemoModalActive: false,
    };
  }

  renderRedirect = () => {
    return (
        <Redirect
            to="/login"
        />
    );
  };

  componentDidMount() {
    fetch("http://82.146.61.253:8585/secure_api_v1/account/validate_token?token=abc6aea2cc32b95ccd49cc4af0934cb24047a6f80879de82")
      .then(res => res.json())
      .then(
        (result) => {
          if (result.success) {
            this.setState({
              authenticated: true,

            });
          } else {
            this.setState({
              authenticated: false,
            });
            this.renderRedirect();
          }
        },
        (error) => {
          this.setState({
            authenticated: false,
          });
          this.renderRedirect();
        }
      )
  }

  render() {
    const { authenticated } = this.state;

    function RouterView() {
      if (authenticated) {
        return <div>
          <div className="text-center">
            <div className="mt-5">
              <img alt='...' className="mb-3" src="http://story-sberbank.ru/media/img/svg/sber_logo.svg" width="222px" height="50px" />
              <a href="#" target="_blank"><span className="navbar-header ml-4 mt-5 mb-4">Process Mining</span></a>
            </div>
              <a href="#"><span className="small text-center link-muted">Демонстрационный режим </span></a>
          </div>


          <div className="row">
            <div className="col px-0">
              <div className="card p-2 mt-3 mb-8 bg-light">
                <div className="card-body p-0">
                  <ul className="nav nav-classic nav-borderless p-0 mb-2 justify-content-center" id="pills-tab" role="tablist">
                    <li className="nav-item" ><Link className="nav-link" to={`/`}>Новое Исследование</Link></li>
                    <li className="nav-item" ><Link className="nav-link active" to={`/researches`}>Мои Исследования</Link></li>
                    <li className="nav-item" ><Link className="nav-link" to={`/notations`}>Контроль Нотаций</Link></li>
                    <li className="nav-item" ><Link className="nav-link" to={`/account`}>Мой Аккаунт</Link></li>
                  </ul>
                  <Switch>
                    <Route path="/" exact component={() => <PostsList />} />
                    <Route path="/notations" exact component={() => <PostsList />} />
                    <Route path="/researches/:id" exact component={FullPost} />
                    <Route path="/researches" exact component={Researches} />
                  </Switch>
                </div>
              </div>
            </div>
          </div>
          </div>

      }
        return <Switch>
          <Route path="/login" exact component={() => <PostsList />} />
          <Route path="/signup" exact component={FullPost} />
          <Route path="/restore" exact component={AddForm} />
        </Switch>;
    }


    return (
      <div className="App">
        <Router>
          <div className="container p-0">
            <RouterView />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
