import React from 'react';
import {Link} from 'react-router-dom';

import './Researches.scss';

const ResearchInfo = () => {
  return (
    <div className="research-info">
      <Link className="btn btn-default btn-sm pb-2 mt-4 mb-2" to="/">← Вернуться к Моим Исследованиям</Link>
      <br />
        <div className="row">
          <div className="col-sm-8">
            <div className="card section-base p-4 mb-4 bg-white">
              <div className="card-body">
                <div className="row ">
                  <div className="col-sm-3 text-center">
                    <img src="research.preview_url" className="img-thumbnail" alt="Research Image" width="150" height="150"></img>
                  </div>
                  <div className="col-sm-9">
                    <div className="row">
                      <!-- Input -->
                      <div className="col-sm-6">
                        <div className="js-form-message">
                          <label id="nameLabel" className="form-label">
                            Название исследования:
                          </label>

                          <div className="form-group">
                            <input type="text" className="form-control form-control-sm" name="name" v-model="research.name" placeholder="Название исследования" required aria-describedby="nameLabel">
                          </div>
                        </div>
                      </div>
                      <!-- End Input -->

                      <!-- Input -->
                      <div className="col-sm-6">
                        <div className="js-form-message">
                          <label id="usernameLabel" className="form-label">
                            <i className="far fa-sm fa-building mr-2"> </i>Название отдела:
                          </label>

                          <div className="form-group">
                            <input type="text" className="form-control form-control-sm" name="username"  v-model="research.department"  placeholder="Название отдела" required aria-describedby="usernameLabel">
                          </div>
                        </div>
                      </div>
                      <!-- End Input -->

                    </div>
                    <div className="row">

                      <div className="col-sm-6">
                        Нотации: <b>{{research.notation_name}}</b>
                        <Link  className="btn btn-sm btn-link float-right" to="'/notations/'+research.notation_id">Редактировать Нотации →</Link>
                    </div>

                    <div className="col-sm-6">

                      <button type="button" className="btn btn-sm btn-primary float-right mt-4"><i className="far fa-save mr-2"></i> Сохранить изменения</button>
                    </div>

                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

    </div>
  <div className="row">
    <div className="col-sm-8">
      <div className="card section-base p-1 mb-4 bg-white">
        <div className="card-title ml-4 mt-4"><b>Граф</b> Процессов</div>
        <div className="card-body">

          <div className="row">
            <div className="col-md-12">



          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <div className="row mb-8">
    <div className="col-sm-8">
      <div className="card section-base p-4 mb-1 bg-white">
        <div className="card-title"><b>Dashboard</b> Процессов</div>
        <div className="card-body">

          <!--<graph2d-->
          <!--id="first_chart"-->

          <!--:items="research.dashboard[0].data"-->
          <!--:options=bar_chart_options-->
          <!--&gt;-->
          <!--</graph2d>-->

          <button type="button" className="btn btn-sm btn-secondary float-right"><i className="fas fa-plus-circle mr-2"></i>  Добавить элемент Dashboard</button>


        </div>
      </div>
    </div>
    <div className="col-sm-4 d-none d-md-block smooth-transition" id="detailsSection" style="transition: margin 700ms ease-out; margin-top: -1098px">
      <div className="card section-base p-1 mb-4 bg-white">
        <div className="card-body">
          <div className="card-title">
            <span><b>Кастомизация данных</b> </span>
          </div>
          <div className="panel-group p-1 " id="accordion">
            <div className="card" style="margin: auto">
              <a style="color: #4A4949" data-toggle="collapse" data-parent="#accordion" href="#price_breakdown">
                <div className="card-header ">
                  Параметры Графа
                </div>
              </a>
              <div id="price_breakdown" className="panel-collapse collapse show">
                <div className="card-body">
                  <small>Число нод:</small> <span className="float-right"> </span>
                  <div className="slidecontainer mb-1">
                    <input type="range" min="1" max="sliders.nodes_max" v-model="sliders.nodes" className="slider" id="nodeRange">
                  </div>
                  <div className="custom-control custom-checkbox d-flex align-items-center text-muted">
                    <input type="checkbox" className="custom-control-input" id="EventCount" name="EventCount" required >
                      <label className="custom-control-label ml-1" for="EventCount">
                        <small>
                          Тотальное число событий
                        </small>
                      </label>
                  </div>
                  <div className="custom-control custom-checkbox d-flex align-items-center text-muted">
                    <input type="checkbox" className="custom-control-input" id="UserCount" name="UserCount" required >
                      <label className="custom-control-label ml-1 mb-5" for="UserCount">
                        <small>
                          Средне число пользователей
                        </small>
                      </label>
                  </div>


                  <small>Число связей:</small> <span className="float-right"> test </span>
                  <div className="slidecontainer">
                    <input type="range" min="1" max="sliders.edges_max" v-model="sliders.edges" className="slider" id="linkRange">
                  </div>
                  <div className="custom-control custom-checkbox d-flex align-items-center text-muted">
                    <input type="checkbox" className="custom-control-input" id="LinkCount" name="EventCount" required >
                      <label className="custom-control-label ml-1" for="LinkCount">
                        <small>
                          Тотальное число связей
                        </small>
                      </label>
                  </div>
                  <div className="custom-control custom-checkbox d-flex align-items-center text-muted">
                    <input type="checkbox" className="custom-control-input" id="AvgLinkTime" name="EventCount" required >
                      <label className="custom-control-label ml-1" for="AvgLinkTime">
                        <small>
                          Среднее время между событиями
                        </small>
                      </label>
                  </div>
                  <div className="custom-control custom-checkbox d-flex align-items-center text-muted">
                    <input type="checkbox" className="custom-control-input" id="MaxLinkTime" name="EventCount" required >
                      <label className="custom-control-label ml-1" for="MaxLinkTime">
                        <small>
                          Максимальное время между событями
                        </small>
                      </label>
                  </div>
                  <div className="custom-control custom-checkbox d-flex align-items-center text-muted">
                    <input type="checkbox" className="custom-control-input" id="MinLinkTime" name="EventCount" required >
                      <label className="custom-control-label ml-1" for="MinLinkTime">
                        <small>
                          Минимальное время между событиями
                        </small>
                      </label>
                  </div>
                  <button onClick="updateGraph" type="button" className="btn btn-sm btn-secondary float-right mt-3 mb-3">Применить</button>
                </div>
              </div>
            </div>
            <div className="card mt-2">
              <a style="color: #4A4949" data-toggle="collapse" data-parent="#accordion" href="#customization">
                <div className="card-header">
                  Фильтрация по Атрибутам
                </div>
              </a>
              <div id="customization" className="panel-collapse collapse ">
                <div className="card-body">

                  <button type="button" className="btn btn-sm btn-secondary float-right mb-3"><i className="fas fa-plus-circle mr-2"></i> Добавить фильтр</button>

                </div>
              </div>
            </div>

            <div className="card mt-2">
              <a style="color: #4A4949" data-toggle="collapse" data-parent="#accordion" href="#more_info">
                <div className="card-header">
                  Детальная информация
                </div>
              </a>
              <div id="more_info" className="panel-collapse collapse ">
                <div className="card-body">

                  Нода: <b>m</b>
                  <br />
                    Число событий: <b>m</b>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    </div>
  );
};

export default ResearchInfo;
