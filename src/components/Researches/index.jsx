import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { axios } from 'core';

import './Researches.scss';


class Researches extends Component{

  constructor(props) {
    super(props);
    this.state = {
      researches: [],
    };
  }

  componentDidMount() {
    console.log(this.state.researches);
    axios.get("researches?token=abc6aea2cc32b95ccd49cc4af0934cb24047a6f80879de82")
      .then(res => {
        this.setState({ researches: res.data });
      })
  }

  render() {
    return (
      <div className="researches">
        <h4 className="pb-2 mt-4 mb-2 border-bottom">Мои Исследования</h4>
        <div className="row">
          <div className="col-sm-12">
            <div className="card section-base mb-6">
              <div className="card-body">
                <div className="row">
                  <div className="col-sm-2 text-center">
                    <img src="research.preview_url" className="img-thumbnail" alt="Listing Image" width="150" height="150"></img></div>
                  <div className="col-sm-5">
                    <span><b>Название исследования</b></span>
                    <br/>
                    <i className="far fa-building"></i>
                    <small>Департтамент кредитования</small>
                    <i className="ml-4 fas fa-user"></i>
                    <small>Стас</small>
                    <i className="ml-4 fas fa-link"></i>
                    <small>Нотации: <Link to="/notations/+research.notation_id">Процессы кредитования</Link></small>
                    <br/>
                  </div>
                  <div className="col-sm-2 mb-4">Проанализировано событий <span
                      className="badge badge-secondary">574,893</span></div>
                  <div className="col-sm-3 text-center"><span><Link onClick="openResearch('test_research_id')"
                                                                    className="btn btn-sm btn-primary"
                                                                    to="'/researches/' + research.research_id">Открыть Исследование</Link>
                <Link className="btn btn-sm btn-danger" to="/">Удалить</Link>
                <br/><small>обновлено 5 мин назад</small></span></div>
                </div>
                <div className="row">
                  <div className="col-sm-2 mt-3 text-center">
                    <span className="mt-2 mb-1 "><span className="small">Статус </span> <span
                        className="badge badge-primary">Готово</span></span>
                    <a href="research.source_file_url" target="_blank"><span
                        className="badge badge-pill badge-light mb-4"><i className="fas fa-sm fa-download"></i> Скачать исходный файл</span></a>
                  </div>
                  <div className="col-sm-3" v-for="stat in research.stats.slice(0, 3)">
                    <div className="card occupancy-section bg-white mb-2">
                      <div className="card-body">Процент успешных действий <span>50</span>
                        <div className="progress mt-2">
                          <div className="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                               role="progressbar"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Researches;
